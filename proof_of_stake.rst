この文書の元は
https://gitlab.com/tezos/tezos/blob/94378f9809e453e46686e8f614a7e712d6d259fd/docs/whitedoc/proof_of_stake.rst
< .. > 内は訳者の註およびコメント。

現在の訳の状態: とりあえず訳しただけで言い回しなどは直訳的で硬い部分がある。
一部の技術的内容については訳者が詳細を知らないため誤訳されている可能性がある
(ただしそのような場所には註釈を付けた)。

.. _proof-of-stake:

.. Proof-of-stake in Tezos
Tezos の Proof-of-stake
=======================

この文書では Tezos の proof-of-stake アルゴリズム、
プロトコル PsYLVpVvgbLhAhoqAkMFUo6gudkJ9weNXhUYCiLDzcUpFpkk8Wt
において採用されているもの、
に関する詳しい説明を行う。

.. This document provides an in-depth description of the Tezos
.. proof-of-stake algorithm as implemented in
.. PsYLVpVvgbLhAhoqAkMFUo6gudkJ9weNXhUYCiLDzcUpFpkk8Wt

.. Blocks
ブロック
------

Tezosブロックチェーンはブロックの結合リストである。
ブロックにはヘッダとオペレーションのリストが入っている。
ヘッダ自身はシェルヘッダ(全てのプロトコルに共通している)と
プロトコルに特化したヘッダを持つ。

.. The Tezos blockchain is a linked list of blocks. Blocks contain a
.. header, and a list of operations. The header itself decomposes into a
.. shell header (common to all protocols) and a protocol specific header.

.. Shell header
シェルヘッダ
~~~~~~~~~~~~

シェルヘッダは次の情報を持っている:

-  ``level``: ブロックの genesis block からの高さ。
-  ``proto``: Genesis からのプロトコルの変更回数 (mod 256)。
-  ``predecessor``: 前ブロックのハッシュ。
-  ``timestamp``: ブロックが作成されたと主張されるタイムスタンプ。
-  ``validation_pass``: 検証パスの数 (オペレーションリストのリストの数でもある)。
-  ``fitness``: Unsigned byte列の列。長さと辞書順による順序を持つ。
   これはこのブロックで終了するチェーンの fitness (確からしさ)を表す。
   <``src/lib_base/fitness.ml``に定義あり。>
-  ``operations_hash``: オペレーションの merkle tree のルートハッシュ値の
   リストのハッシュ。<リストのハッシュになるのは>検証パス毎に一つのオペレーションリストがあるため。
-  ``context``: このブロックを適用した後のコンテキスト状態のハッシュ。

.. The shell header contains
.. 
.. -  ``level``: the height of the block, from the genesis block
.. -  ``proto``: number of protocol changes since genesis (mod 256)
.. -  ``predecessor``: the hash of the preceding block.
.. -  ``timestamp``: the timestamp at which the block is claimed to have
..    been created.
.. -  ``validation_pass``: number of validation passes (also number of
..    lists of lists of operations)
.. -  ``fitness``: a sequence of sequences of unsigned bytes, ordered by
..    length and then lexicographically. It represents the claimed fitness
..    of the chain ending in this block.
.. -  ``operations_hash`` The hash of a list of root hashes of merkle
..       trees of operations. There is one list of operations per
..       validation pass
.. -  ``context`` Hash of the state of the context after application of
..    this block.

.. Protocol header (for tezos.alpha):
プロトコルヘッダ(tezos.alpha固有)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  ``signature``: (デジタル署名そのものを除いたものに対する)シェルとプロトコルヘッダのデジタル署名
-  ``priority``: ブロックがベークされた際のデリゲートの優先順位
-  ``seed_nonce_hash``: 乱数へのコミット。チェーンへのエントロピー生成に使われる。
   (``BLOCKS_PER_COMMITMENT`` = 32) ブロックに1ブロックのみにだけ現れる。
-  ``proof_of_work_nonce``: このブロックでスパム防止のために行われた低難度 proof-of-work を通過するために使われた nonce。

.. -  ``signature``: a digital signature of the shell and protocol headers
..    (excluding the signature itself).
.. -  ``priority``: the position in the priority list of delegates at which
..    the block was baked.
.. -  ``seed_nonce_hash``: a commitment to a random number, used to
..    generate entropy on the chain. Present in only one out of
..    (``BLOCKS_PER_COMMITMENT`` = 32) blocks.
.. -  ``proof_of_work_nonce``: a nonce used to pass a low-difficulty
..    proof-of-work for the block, as a spam prevention measure.

.. Block size
ブロックサイズ
~~~~~~~~~~

Tezos はブロックの全体をただちにダウンロードすることはなく、
ヘッダと各オペレーションリストを別に取り扱う。
Tezos.alpha では、トランザクションリストへの最大サイズ制限は
``MAX_TRANSACTION_LIST_SIZE`` = 500kB (これは各10分で最大5MBとなる)である。

.. Tezos does not download blocks all at once, but rather considers headers
.. and various lists of operations separately. In Tezos.alpha, a maximum
.. size in bytes is applied to the list of transactions
.. ``MAX_TRANSACTION_LIST_SIZE`` = 500kB (that's 5MB every 10 minutes at
.. most).

他の種類のオペレーション(裏書き、告発、開示)はオペレーション数の上限が設けられている
<?実際の数値は?>。(しかしながら、オペレーション全体へのサイズリミットも
念のために導入されている。<?では実際には?>)

.. Other lists of operations (endorsements, denunciations, reveals) are
.. limited in terms of number of operations (though the defensive
.. programming style also puts limits on the size of operations it
.. expects).

この区別により、合意形成に重要なオペレーションはトランザクションとブロックスペースを競合しない。

.. This ensure that consensus critical operations do not compete with
.. transactions for block space.

.. Delegation
デリゲーション
----------

Tezos.alphaはdelegated proof-of-stakeモデルを採用している。
DPOSの略語は例えばBitsharesで使われているような特定のアルゴリズムを指すもので、
これはTezos.alphaで使用されているモデルとは *異なる* 。しかしながらTezos.alphaにも
デリゲーションの概念は存在する。<Tezos.alphaのモデルはDPOSと区別するため
最近はLPOS(Liquid Proof-Of-Stake)と自称している。>

.. Tezos.alpha uses a delegated proof-of-stake model. The acronym DPOS has come to
.. designate a specific type of algorithm used, for instance in Bitshares.
.. This is *not* the model used in Tezos.alpha, though there is a concept
.. of delegation.

.. Delegate
デリゲート
~~~~~~~~~

Tezos.alphaでは、トークンは *マネージャーキー* と呼ばれる秘密鍵によってコントロールされる。
Tezos.alphaのアカウントではマネージャーは公開デリゲートキーを指定できる。
このキーはマネージャー自身のものであってもよいし、第三者のものであってもよい。
デリゲートの責任はproof-of-stake合意アルゴリズムとTezosのガバナンスに参加することである。

.. In tezos.alpha, tokens are controlled through a private key called the
.. *manager key*. Tezos.alpha accounts let the manager specify a public
.. delegate key. This key may be controlled by the manager themselves, or
.. by another party. The responsibility of the delegate is to take part in
.. the proof-of-stake consensus algorithm and in the governance of Tezos.

一般的にマネージャはいつでもデリゲートを変更する事が可能だ。
しかしながら、コントラクトはデリゲート変更不可であるとマークをつけることもできる。
デリゲーションは動的に変化可能だが、この変化は数サイクル後<正確には？>にならないと
効力を発揮しない。

.. The manager can generally change the delegate at any time, though
.. contract can be marked to specify an immutable delegate. Though
.. delegation can be changed dynamically, the change only becomes effective
.. after a few cycles.

Tezosにはデフォルトアカウント、単なる公開鍵のハッシュ、も存在する。
これらのアカウントはデリゲートキーを持たないし、
proof-of-stakeアルゴリズムに参加することもない。

.. There are also default accounts in Tezos, which are just the hash of the
.. public key. These accounts do not have an attached delegate key and do
.. not participate in the proof-of-stake algorithm.

最後に、(安全保証金を置くのに使用される)デリゲートアカウントはデリゲート自身にデリゲートされる。

.. Finally, delegate accounts (used for placing safety deposits) are
.. automatically delegated to the delegate itself.

.. Active and passive delegates
デリゲートのactive, passive
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

デリゲートはactiveもしくはpassiveのいずれかの状態にある。
Passiveデリゲートはベーキングと裏書きに選ばれることはない。

.. A delegate can be marked as either active or passive. A passive delegate
.. cannot be selected for baking or endorsement.

サイクル ``n`` において、デリゲートは、もし過去 ``CYCLES_BEFORE_DEACTIVATION`` = 5 サイクル、つまりここではサイクル ``n-1`` 、 ``n-2`` , ..., ``n - CYCLES_BEFORE_DEACTIVATION`` において、ブロック生成もしくは裏書きに失敗した場合、もしくは安全保証金に変化がなかった場合、
passiveとなる。

.. A delegate becomes passive for cycle ``n`` when they fail to create any
.. of the blocks or endorsements in the past ``CYCLES_BEFORE_DEACTIVATION``
.. = 5 cycles, or to change their security deposit. So, in this case, in
.. cycles ``n-1``, ``n-3``, ..., ``n - CYCLES_BEFORE_DEACTIVATION``.

十分にブロック生成や裏書き機会が得られないためpassiveとなってしまうことを
心配する規模の小さいデリゲートは、2サイクル毎に少額の意味のないトランザクションを
安全保証金に関して行うことで、これを避けることができる。

.. A small delegate who is afraid they might be deactivated because they
.. were not given the opportunity to create any block or endorsement can
.. ensure they do not become deactivated by making small, meaningless
.. transactions with their security deposits once every two cycles.

議論: 小さい ``CYCLES_BEFORE_DEACTIVATION`` を採用すると、
チェーンはより素早く退場した参加者に対応できる。<もはや存在しないデリゲートに対してブロック生成権や裏書き権を配分しなくて済むため、ブロック生成時間などがより安定する。>
これはBitcoinの「難易度調整」とは異なるものである。
しかしながら、長い値を与えると、長期間では少数フォークは多数フォークよりも遅く成長する。<???>
``CYCLES_BEFORE_DEACTIVATION`` は多数チェーンに「優先スタート」権を与える。

.. Discussion: giving ``CYCLES_BEFORE_DEACTIVATION`` a small value means
.. the chain adapts more quickly to participants disappearing. It's not
.. unlike the "difficulty adjustment" of Bitcoin. However, a long value
.. would ensure that a minority fork progresses more slowly for a longer
.. period of time than the majority fork. ``CYCLES_BEFORE_DEACTIVATION``
.. gives the majority chain a "headstart".

このデリゲートのactive、passiveはプロトコル変更の投票権には影響しない。

.. This does not affect voting rights for protocol changes.

.. Rolls
ロール
~~~~~

<ロールとはcoin roll、すなわち貨幣を束にして包装した棒金を意味するが、
「包装硬貨」としても「棒金」としても意味が通じにくいと思われるため、単にロールと
呼ぶことにする。>

理論的には全てのトークンにシリアル番号を割り振って、どのトークンがどのデリゲートに
所有されているか追跡することは可能である。しかし、このような粒度で所有追跡
するのはノードにとって過負荷になる。
その代わり、我々はロールの概念を導入する。ロールはあるキーにデリゲートされたコインの集合を表す。
トークンが移動した際、もしくはコントラクトのデリゲートが変更された際、
次のアルゴリズムに沿ってロールに結び付けられたデリゲートは変化する。

.. In theory, it would be possible to give each token a serial number, and
.. track the specific tokens assigned to specific delegates. However, it
.. would be too demanding of nodes to track assignment at such a granular
.. level. Instead we introduce the concept of rolls. A roll represents a
.. set of coins delegated to a given key. When tokens are moved, or a
.. delegate for a contract is changed, the rolls change delegate according
.. to the following algorithm.

各デリゲートはロールIDのスタックといくらかの常に ``TOKENS_PER_ROLLS`` よりも小さい「端数」
を持っている。
トークンがあるデリゲートから別のデリゲートに移される時、まず、端数が使われる。
もし端数では足りない場合、いくつかのロールが「崩され」る必要がある。
その場合、デリゲートのロールスタックからそれらが取り除かれ、グローバルの未割り当てロールスタックに移される。これは必要な額が得られるまで続けられ、
幾らかの端数が(必要がある場合には)残ることになる。

.. Each delegate has a stack of roll ids plus some "change" which is always
.. an amount smaller than ``TOKENS_PER_ROLLS``. When tokens are moved from
.. one delegate to the other, first, the change is used. If it is not
.. enough, rolls need to be "broken" which means that they move from the
.. delegate stack to a global, unallocated, roll stack. This is done until
.. the amount is covered, and some change possibly remains.

次に、他方のデリゲートにトークンが移動する。まずトークンは「端数」に加えられる。
もしこの合計が ``TOKENS_PER_ROLLS`` よりも大きければ、グローバルの未割り当てロールスタックからロールが取り出されデリゲートスタックに付け加えられる。
もし、グローバルスタックが空の場合は、新しいロールが生成される。

.. Then, the other delegate is credited. First the amount is added to the
.. "change". If it becomes greater than ``TOKENS_PER_ROLLS``, then rolls
.. are unstacked from the global unallocated roll stack onto the delegate
.. stack. If the global stack is empty, a fresh roll is created.

これにより、もしデリゲートが数回のトランザクションを通じて変化したとき、
もしたとえそれぞれのオペレーションがロールに満たないトークンの移動であったとしても、
ロールの割り当ては保存されるという性質がある。<???>
<スタックの性質から、崩されたロールはすみやかに新しいロールに使われる。>

.. This preserves the property that if the delegate is changed through
.. several transactions, the roll assignment is preserved, even if each
.. operation moves less than a full roll.

このトークン追跡の利点は、
悪意あるフォークを作ろうとしているデリゲートにとっては、
もし所有するトークンをうまく操作したとしても、
簡単には特定のロールを自分に割り当てる<そしてベーキング権や裏書き権を得る>ことができないことにある。
<自分の持つあるアカウントから別のアカウントにトークンを移動し、
ロール構成を変化させたとしても、別のアカウントに割り当てられるロールのIDは元のロールのものと変わらない。>

.. The advantage of tracking tokens in this way is that a delegate creating
.. a malicious fork cannot easily change the specific rolls assigned to
.. them, even if they control the underlying tokens and shuffle them
.. around.

<現在、>1ロールには ``TOKENS_PER_ROLLS`` = 10,000 トークンが必要で、
Tezos財団が計画したgenesisブロックでは約80,000のロールが存在するはずだ。
しかし、ロールの総数はインフレーションやデリゲーションへの参加によって増大するだろう。

.. Rolls hold ``TOKENS_PER_ROLLS`` = 10,000 tokens and thus there should be
.. about 80,000 rolls in the Tezos foundation's planned genesis block,
.. though the number of rolls will increase with inflation and / or
.. participation in the delegation.

.. Roll snapshots
ロールスナップショット
~~~~~~~~~~~~~~

ロールスナップショットはあるブロックのロールの状態を表す。
ロールスナップショットは ``BLOCKS_PER_ROLL_SNAPSHOT`` = 256 ブロック毎に、
つまり1サイクルに16回生成される。
ここにはメモリ使用と経済的効率の間のトレードオフが存在する。
つまり、もしロールスナップショットがあまりに頻繁に作られるようであれば、
それには多くのメモリを必要とするだろうし、
もし逆に少なすぎれば、ロールを多く得るために、スナップショットが取られる前に大量のトークンを購入し、直後に売却する、という戦略が取られてしまう。

.. Roll snapshots represent the state of rolls for a given block. Roll
.. snapshots are taken every ``BLOCKS_PER_ROLL_SNAPSHOT`` = 256 blocks,
.. that is 16 times per cycle. There is a tradeoff between memory
.. consumption and economic efficiency. If roll snapshots are too frequent,
.. they will consume a lot of memory. If they are too rare, strategic
.. participants could purchase many tokens in anticipation of a snapshot
.. and resell them right after.

.. Cycles
サイクル
------

Tezos.alphaブロックチェーンでのブロックは ``BLOCKS_PER_CYCLE`` = 4,096ブロックからなる
*サイクル* にグループ分けされる。
ブロックは最低 ``TIME_BETWEEN_BLOCKS`` = 1分 離れているので、1サイクルは
*最低* 2日20時間16分である。
以降、現在のサイクルを、チェーンの開始時からの n 番目のサイクルであることをもって
``n`` で表すこととする。
サイクル ``(n-1)`` は現在のサイクルの直前のサイクルであり、サイクル ``(n-2)`` はもう一つ手前のもの、そしてサイクル ``(n+1)`` は次のサイクル、などとする。

.. Blocks in the Tezos.Alpha Blockchain are grouped into *cycles* of
.. ``BLOCKS_PER_CYCLE`` = 4,096 blocks. Since blocks are at least
.. ``TIME_BETWEEN_BLOCKS`` = one minute apart, this means a cycle lasts *at
.. least* 2 days, 20 hours, and 16 minutes. In the following description,
.. the current cycle is referred to as ``n``, it is the nth cycle from the
.. beginning of the chain. Cycle ``(n-1)`` is the cycle that took place
.. before the current one, cycle ``(n-2)`` the one before, cycle ``(n+1)``
.. the one after, etc.

どの時点でも、Tezosシェルは``PRESERVED_CYCLES`` = 5サイクル
(これは *最低* 14日5時間20分である)より過去の時点からフォークするブランチを自動的には受けつけない。

.. At any point, the tezos shell will not implicitly accept a branch whose
.. fork point is in a cycle more than ``PRESERVED_CYCLES`` = 5 cycles in the
.. past (that is *at least* 14 days, 5 hours, and 20 minutes).

.. Security deposits
安全保証金
~~~~~~~~~~~~~~~~~

安全保証金の値段は各ブロックにつき ``BLOCK_SECURITY_DEPOSIT`` = 512 XTZ、
各裏書きにつき ``ENDORSEMENT_SECURITY_DEPOSIT`` = 64 XTZ である。

.. The cost of a security deposit is ``BLOCK_SECURITY_DEPOSIT`` = 512 XTZ
.. per block created and ``ENDORSEMENT_SECURITY_DEPOSIT`` = 64 XTZ per
.. endorsement.

各デリゲートキーは安全保証金アカウントと結びついている。
デリゲートがブロックをベークするか裏書きする際には、安全保証金が自動的に
保証金アカウントから差し引かれ ``PRESERVED_CYCLES`` サイクルの間凍結される。
その後、保証金は<没収されていない限り>ベーカーのメインアカウントに自動的に払い戻される。

.. Each delegate key has an associated security deposit account.
.. When a delegate bakes or endorses a block the security deposit is
.. automatically moved to the deposit account where it is frozen for
.. ``PRESERVED_CYCLES`` cycles, after which it is automatically moved
.. back to the baker's main account.

保証金は ``PRESERVED_CYCLES`` サイクルの間ロックされるので、
ある時点で、全トークンの約
((``BLOCK_SECURITY_DEPOSIT`` +
``ENDORSEMENT_SECURITY_DEPOSIT`` \* ``ENDORSERS_PER_BLOCK``) \*
(``PRESERVED_CYCLES`` + 1) \* ``BLOCKS_PER_CYCLE``) / ``763e6`` = 8.25% 
が安全保証金として固定されることがわかる。
これはまた、デリゲートがブロック生成機会を逃さないためには、
自分にデリゲートされたトークン総量の 8.25% 以上の量のトークンを実際に所有すべきである
ということでもある。

.. Since deposits are locked for a period of ``PRESERVED_CYCLES`` one can
.. compute that at any given time, about ((``BLOCK_SECURITY_DEPOSIT`` +
.. ``ENDORSEMENT_SECURITY_DEPOSIT`` \* ``ENDORSERS_PER_BLOCK``) \*
.. (``PRESERVED_CYCLES`` + 1) \* ``BLOCKS_PER_CYCLE``) / ``763e6`` = 8.25% of
.. all tokens should be held as security deposits. It also means that a
.. delegate should own over 8.25% of the amount of token delegated to them
.. in order to not miss out on creating any block.

.. Baking rights
ベーキング権
~~~~~~~~~~~~~

Tezos.alphaにおいて、ブロックのサインと公開をベーキングと呼ぶ。
Bitcoinにおいては、ブロックの公開権はproof-of-workパズルを解くことと紐づいている。
Tezos.alphaでは、サイクル ``n`` におけるブロック公開権は、
サイクル ``n-PRESERVED_CYCLES-2`` からランダムに選択されたロールスナップショットから
ランダムに選択されたロールに与えられる。

.. Baking in tezos.alpha is the action of signing and publishing a block.
.. In Bitcoin, the right to publish a block is associated with solving a
.. proof-of-work puzzle. In tezos.alpha, the right to publish a block in
.. cycle ``n`` is assigned to a randomly selected roll in a randomly
.. selected roll snapshot from cycle ``n-PRESERVED_CYCLES-2``.

現在のところ、プロトコルはランダムシードを各サイクルで生成する。
このランダムシードを使って、CSPRNG<Cryptographically Secure Pseudo Random Number Generator>からそのサイクルのベーキング権利をランダムに割り当てる。

.. We admit, for the time being, that the protocol generates a random seed
.. for each cycle. From this random seed, we can seed a CSPRNG which is
.. used to draw baking rights for a cycle.

サイクルの各ポジション<blockのこと？>にデリゲートの順序づけされたリストが紐づけられる。
これはactiveなロールのセットから置き換えによってランダムに生成されるため、
結果、同じパブリックキーが複数回りストに現れることもありうる。
リストの最初のベーカーがそのレベルのブロックをベークする者となる。
もしそのデリゲートが何らかの理由でベークできなかった場合、<xxx ベー => ベー>
リストの次のデリゲートがブロックをベークする。

.. To each position, in the cycle, is associated a priority list of
.. delegates.
.. This is drawn randomly, with replacement, from the set of active rolls
.. so it is possible that the same public key appears multiple times in
.. this list.
.. The first baker in the list is the first one who can bake a block at
.. that level.
.. If a delegate is for some reason unable to bake, the next delegate in
.. the list can step up and bake the block.

最高優先度を持つデリゲートは
``timestamp_of_previous_block`` + (``TIME_BETWEEN_BLOCKS`` = 1分)
より後のタイムスタンプを持つブロックをベークできる。
k番目の優先度を持つ者は、 ``k * TIME_BETWEEN_BLOCKS`` = k分以降後である。

.. The delegate with the highest priority can bake a block with a timestamp
.. greater than ``timestamp_of_previous_block`` plus
.. ``TIME_BETWEEN_BLOCKS`` = one minute. The one with the kth highest
.. priority, ``k * TIME_BETWEEN_BLOCKS`` = k minutes.

ブロックをベークすると、ブロック報酬として ``BLOCK_REWARD`` = 16 XTZ と、
ブロック中のトランザクションに指定された全てのフィーを獲得できる。

.. Baking a block gives a block reward of ``BLOCK_REWARD`` = 16 XTZ plus
.. all fees paid by transactions inside the block.

.. Endorsements
裏書き
~~~~~~~~~~~~

各ベーキングスロットにつき、 ``ENDORSERS_PER_BLOCK`` = 32の *裏書人* を紐づける。 
裏書人はデリゲートの集合からランダムに選んばれる<with replacement とは？>。

.. To each baking slot, we associate a list of ``ENDORSERS_PER_BLOCK`` = 32
.. *endorsers*. Endorsers are drawn from the set of delegates, by randomly
.. selecting 32 rolls with replacement.

各裏書人は最後にベークされたレベル ``n`` のブロックを検証し、
裏書きオペレーションを発行する。
裏書きオペレーションは ``n+1`` のブロックにベークされブロック ``n`` の
`fitness` に寄与する。一旦ブロック ``n+1`` がベークされると、
ブロック ``n`` の他の裏書きは全て無効となる。

.. Each endorser verifies the last block that was baked, say at level
.. ``n``, and emits an endorsement operation. The endorsement operations
.. are then baked in block ``n+1`` and will contribute to the `fitness`
.. of block ``n``. Once block ``n+1`` is baked, no other endorsement for
.. block ``n`` will be considered valid.

裏書人は(ブロック生成者が報酬を受け取るのと同時に)報酬を受け取る。
報酬額はブロック優先度が1の場合は ``ENDORSEMENT_REWARD`` = 2 / ``BLOCK_PRIORITY`` である。裏書きされるブロックが優先度2のベーカーによるものの場合は裏書報酬はこの半分だけとなる。
<では3以降はどうなのか？>

.. Endorsers receive a reward (at the same time as block creators do). The
.. reward is ``ENDORSEMENT_REWARD`` = 2 / ``BLOCK_PRIORITY`` where block
.. priority starts at 1. So the endorsement reward is only half if the
.. block of priority 2 for a given slot is being endorsed.

同じブロックに対し ``k`` 回同じ裏書人が選ばれることはありうる。この場合、
``k`` 分の保証金が必要となり、 ``k`` 分の報酬が支払われる。
しかしながら、同じブロックを ``k`` 回裏書するためにネットワークに送る必要のある
オペレーションは一つだけで良い。

.. It is possible that the same endorser be selected ``k`` times for the
.. same block, in this case ``k`` deposits are required and ``k`` rewards
.. gained. However a single operation needs to be sent on the network to
.. endorse ``k`` times the same block.

Fitness
~~~~~~~

各ブロックには`fitness`と呼ばれる測度が紐づけられていて、
これはこのブロックへとつながるチェーンの質を表す。
この測度<xxx measure>はBitcoinでは単にチェーンの長さであるが、
Tezosでは、それに各ブロックの裏書きの数を足したものになる。
ある fitness ``f`` を持つレベル ``n`` のブロックがあったとして、
ブロック ``n`` に対して ``e`` の裏書きを持つ新しい先頭ブロックを受け取った時、
新しい先頭ブロックの fitness は ``f+1+e`` となる。

.. To each block we associate a measure of `fitness` which determines the
.. quality of the chain leading to that block.
.. This measure in Bitcoin is simply the length of the chain, in Tezos we
.. add also the number of endorsements to each block.
.. Given a block at level ``n`` with fitness ``f``, when we receive a new
.. head that contains ``e`` endorsements for block ``n``, the fitness of
.. the new head is ``f+1+e``.

.. Inflation
インフレーション
~~~~~~~~~~~

ブロック報酬と裏書き報酬は最大
``ENDORSERS_PER_BLOCK`` \* ``ENDORSEMENT_REWARD`` + ``BLOCK_REWARD`` =
80 XTZ である。
これは最大年 5.51% のインフレーションを引き起こす。

.. Inflation from block rewards and endorsement reward is at most
.. ``ENDORSERS_PER_BLOCK`` \* ``ENDORSEMENT_REWARD`` + ``BLOCK_REWARD`` =
.. 80 XTZ. This means at most 5.51% annual inflation.

.. Random seed
ランダムシード
~~~~~~~~~~~

サイクル ``n`` にはランダムシードが一つ紐づけられる。
これは256bitの数で、サイクル ``(n-PRESERVED_CYCLES-1)`` の最後に、
サイクル ``(n-PRESERVED_CYCLES-2)`` 中、32ブロック毎に一回なされた
コミットメントから生成される。

.. Cycle ``n`` is associated with a random seed, a 256 bit number generated
.. at the end of cycle ``(n-PRESERVED_CYCLES-1)`` using commitments made during
.. cycle ``(n-PRESERVED_CYCLES-2)``, in one out of every
.. ``BLOCKS_PER_COMMITMENT`` = 32 blocks.

このコミットは   
サイクル ``(n-PRESERVED_CYCLES-1)`` 中にコミットを行なったベーカーによって
開示されなければならない。これを怠った場合、ペナルティとして、
そのシードコミットを含むブロックのブロック生成報酬とトランザクションフィーは没収される
(ただし、安全保証金は没収されない)。

.. The commitment must be revealed by the original baker during cycle
.. ``(n-PRESERVED_CYCLES-1)`` under penalty of forfeiting the rewards and
.. fees of the block that included the seed commitment (the associated
.. security deposit is not forfeited).

*開示* はオペレーションであって、複数の開示が一つのブロックに含まれることがありうる。
ベーカーは開示をブロックに含めることで、 ``seed_nonce_revelation_tip`` = 1/8 XTZ の報酬を得られる。
開示は無料のオペレーションなので、他のトランザクションとブロックスペースで競合しない。
1ブロックの中には最大 ``MAX_REVELATIONS_PER_BLOCK`` = 32 の開示を含めることができる。
よって、1サイクル中の 1 / (``MAX_REVELATIONS_PER_BLOCK`` \* ``BLOCKS_PER_COMMITMENT``) = 1/1024 のブロックがあれば全ての開示を記録するのに十分である。<xxx 等式理解していない>

.. A *revelation* is an operation, and multiple revelations can thus be
.. included in a block. A baker receives a ``seed_nonce_revelation_tip`` =
.. 1/8 XTZ reward for including a revelation.
.. Revelations are free operations which do not compete with transactions
.. for block space. Up to ``MAX_REVELATIONS_PER_BLOCK`` = 32 revelations
.. can be contained in any given block. Thus, 1 /
.. (``MAX_REVELATIONS_PER_BLOCK`` \* ``BLOCKS_PER_COMMITMENT``) = 1/1024 of
.. the blocks in the cycle are sufficient to include all revelations.

   
開示は、サイクル ``(n-PRESERVED_CYCLES-1)`` の最後にまとめてハッシュ値を取られ、
そこからランダムシードが生成される。
<より具体的には、>サイクル ``(n-PRESERVED_CYCLES-2)`` のシードとある定数、それから
``(n-PRESERVED_CYCLES-1)`` の開示値を合わせたもののハッシュ値が計算される。
得られた新しいシードは保存され、サイクル ``n`` で使われる。

.. The revelations are hashed together to generate a random seed at the
.. very end of cycle ``(n-PRESERVED_CYCLES-1)``.
.. The seed of cycle ``(n-PRESERVED_CYCLES-2)`` is hashed with a constant
.. and then with each revelation of cycle ``(n-PRESERVED_CYCLES-1)``.
.. Once computed, this new seed is stored and used during cycle ``n``.

.. Accusations
告発
-----------

もしデリゲートにより、二つ<以上>の裏書きが、同じスロット<?>もしくは同じレベルの二つ<以上>のブロックに対して行われた場合、告発者はこの証拠を収集し、現サイクルを含む PRESERVED_CYCLES 内のブロックに含めることができる。

.. If two endorsements are made for the same slot or two blocks at the same
.. height by a delegate, the evidence can be collected by an accurser and included
.. in a block for a period of PRESERVED_CYCLES, including the current cycle.

この告発は安全保証金と、その<告発した>時までの報酬の全体を没収する。
没収額の半分は破壊される。残りの半分は告発者にブロック生成報酬の一部として還元される。

.. This accusation forfeits the entirety of the safety deposit and future reward up
.. to that point in the cycle. Half is burned, half goes to the accuser in the form
.. of a block reward.

現在のプロトコルでは、 *同じ* 不正に対し複数の告発がなされる可能性がある。
これはサイクル全体での保証金と報酬が、つまり、不正以降になされた保証金支払いと報酬全てを含む、
没収されるということを意味する。<???以降、は理解できる。全体、の意味がわからない>
<告発されたベイカーAが、それ以降の同サイクル内のブロック ``n`` でベイキングもしくは裏書きした場合、その以降のブロック生成者Bは同じ告発を行うことでブロック ``n`` でのAの保証金と報酬の半分を労せず取得することができる。>

.. In the current protocol, accusations for the *same* incident can be made several
.. times after the fact. This means that the deposits and rewards for the entire
.. cycle are forfeited, including any deposit made, or reward earned, after
.. the incident.

実利的には、あるサイクルでダブルベーキングや二重裏書きをしてしまったベーカーは、
即座にそのサイクル内でのベーキングと裏書きを中止すべきである。

.. Pragmatically, any baker who either double bakes or endorses in a given cycle
.. should immediately stop both baking and endorsing for the rest of that cycle.
