<!--
LANG= pandoc  --pdf-engine=lualatex -o words.pdf words.md
-->
---
header-includes:
    -   \usepackage{amsmath}
geometry: margin=1in
documentclass: ltjarticle
papersize: a4
fontsize: 11pt
title: "Tezos関連用語集"
author: 古瀬 淳
date: \today
---

Amending
:   改訂、修正

Amendment
:   改訂案

Ledger
:   台帳

Self amending crypto ledger
:   自己修正暗号台帳

Purely functional
:   純粋関数型

Seed protocol
:   開始(seed)プロトコル。Seed protocolが実装上の何を指すのか不明。Genesisというprotocolが実装上存在するので、genesisプロトコルとは訳出しない。

Valid
: 正当

Invalid
: 不正

Canonical
: 正準

Tree
: ツリー、木

Mining
: マイニング (採掘でもよいが、マイニングに統一する)。TezosではMiningはBakingと呼ばれる。

Miner
: マイナー(採掘者でもよいが、マイナーに統一する)。TezosではMinerはBakerと呼ばれる。

Baking
: ベイキング。TezosではMiningをBakingと呼ぶ。

Baker
: ベイカー。TezosではMinerをBakerと呼ぶ。

Context
: コンテクスト(コンテキストにあらず)

Operation
: 操作、オペレーション

Transaction
: トランザクション

Constitutionality
: 合憲性

Bond
: 担保。Tezosの場合はマイニングと署名の保証金を指す。

Approval voting
: 承認投票。用語。

Quorum
: 定足率

Stakeholder
: ステークホルダー。Tezosでは明らかにstakeholderはコイン保持者なので、利害関係者と抽象的に訳さない方が良い。

Signer
: 署名者

Roll
: ロール

Preimage
: 原像計算。Preimage attackは原像攻撃。

Denunciation
: 告発。Tezosでは二重署名や二重マイニングを指摘すること。

Minting
: 鋳造。Mining との違いは？

Contract
: コントラクト。

Origination
: オリジネーション。

Coordination issue, coordination problem
: 調整問題。ゲーム理論の用語。

Memory-hard
: Memory-hard http://www.hashcash.org/papers/dagger.html

Focal point
: フォーカルポイント。ゲーム理論用語。期待値の焦点とも。

Unspent output
: 未使用出力。BitcoinのUTXOのこと。

Double spending
: 二重使用

Coin base
: 採掘報酬

Trustlessness
:  トラストレス性。誰も信用する必要のないこと

Setup
: CRSにおける「セットアップ」 https://en.wikipedia.org/wiki/Common_reference_string_model

Nonce
: Nonce。Bakerが秘密登録し、あとで公開する(reveal)義務のある数字。ランダムシード生成に使われる。

Functional language
: 関数型言語

Formal verification
: 形式検証。 形式的検証が正しい用語だが、「形式的」という日本語には「形だけの、おざなりな」という意味があり、「おざなりな検証」と理解する人がいるため、「的」を使わないこととした。

Faucet
: Alphanet、Zeronet用のトークンを生成するサービスおよびツール。

KT1 address, KT1 contracts
: ???

