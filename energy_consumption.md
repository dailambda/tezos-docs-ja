<!--
$ pandoc  --pdf-engine=lualatex --variable urlcolor=blue -o tezos_tech_info.pdf tezos_tech_info.md
-->
---
header-includes:
    -   \usepackage{amsmath}
geometry: margin=1in
documentclass: ltjarticle
papersize: a4
fontsize: 11pt
title: "Tezosのマイニングエネルギー効率について"
author: 古瀬 淳
date: \today
---

Tezosでマイニングを行うにあたっての費用について、PoWとの比較を簡単に行います。

## PoW

### 概要

PoWでは各マイナーが計算量を提供することでネットワークを維持され、
提供した計算量に比例した報酬がネットワーク内通貨で支払われます。
計算量とそれにかかる電気エネルギー量はほぼ比例しますから、
結局は、ある一時点での報酬は、使用した電気量と比例することになります。

報酬額はネットワークに参加するマイナー達の計算能力によって動的に変化します。
マイナー達は競争するので、大雑把に言うと、長期的には報酬は優秀なマイナー達の
ランニングコストに少し上乗せした額に収束します。。

### マイナーの戦略

このため、マイナーが多くの利益を得るためには、

* 大量の計算資源を用意して計算量を確保する
* 計算にかかる電気量が少ない機材を使用するために設備投資する
* 電気料金を軽減させるため、電気料金の安い地域で集中的にマイニングを行う

などの努力が必要です。

### 数字

具体的な数字は、その都度都度のコインの価格とハッシュレートに
依存するので非常にムラがありますが、Bitcoinでの例を挙げます。

https://99bitcoins.com/bitcoin-mining/bitcoin-mining-hardware/#prettyPhoto
によれば(この情報の正しさは私は知りません)、この中で一番よいパフォーマンスを持つ EBIT E11++というASICでは次のような数字が2019-01-13に出ています:

Hash Rate
:    44TH/s
電力
:    2000W = 1440KWh/month
一ヶ月での報酬
:    0.04620054BTC = 18414円 (2018-01-15現在)

損益分岐点は、1kWh = 12.78円 = 12米セントあたりです。
日本でのkWhあたりの公共電気料金は20円を超えますので、
余剰電力を持っていないかぎり、国内ではお話になりません。
なお、世界各国での電気料金の比較は https://en.wikipedia.org/wiki/Electricity_pricing にあります。

### その他の費用

電気料金以外にも、ASICへの投資、相場の乱高下による採算ラインの変化へのリスクヘッジなどを考えると、PoWを行うコストはより高くなります。

## Tezos(LPoS)

### 概要

Tezos(LPoS)でもネットワークの維持には、ブロック生成のために一定の計算量が必要ですが、
PoWのようにブロックの生成にマイナー達が競争をすることは通常ありません。
そのため、ブロック生成にかかるコストが競争により釣り上がることはありません。

マイナーが各ブロックの生成者に選ばれる確率は、そのマイナーが所有するTezosの量に比例します。
ある特定のブロックをどのマイナーが生成するかは、事前に乱数によって決めらます。
(どのように操作されにくい乱数を生成するか、選ばれたマイナーがマイニングを行わなかった
場合どうするか、ありますが、別の話題なのでここでは省略します。)

通常、あるブロックを生成するのはたった一つのマイナーです。生成されたブロックを
32の乱数で選ばれたマイナー達が承認します。ブロックが生成されると生成者はトランザクション
フィーとブロック生成報酬を、承認者達は承認報酬を受け取ります。

### 保証金

ブロック生成での競争が無く、ブロック生成権の無い場合に無駄な計算をしなくてよいため、
ブロック生成にかかる計算量はとても小さいです。これは逆に、悪意のあるなしにかかわらず、
ダブルマイニングなどが簡単にできてしまうことを意味します。(PoWでは計算量が多すぎるため、
ダブルマイニングを意図せず行ってしまうことはまずない。)

ダブルマイニングを避けるため、ブロック生成者は生成時に保証金をネットワークに預ける
必要があります。この保証金はブロックの正しい生成が確認されたのち、(現在は3日後)返還されます。
ダブルマイニング、ダブルサイニングを行ったマイナーはその保証金を没収されます。
ダブルマイニングを行ってしまうような誤ったマイナーの運用をしてそのままマイナーを走らせ
続けると保証金の没収により急速に資産を失うので、注意が必要です。

### デリゲーションサービス

大量のコインを所有するとブロック生成機会が比例して増えるのでマイニング報酬も比例しますが、
相場変動にもさらされます。これを避けるために、デリゲーションサービスを行うことで、
自らが所有しなければいけないポジションを減らすことができます。

Tezosのマイニングは後述するように特殊な計算資源を必要としませんが、それでも一定の
投資と技術を必要とします。そのような投資を難しいと考えるTezos所有者はその所有するコインの
マイニング権をマイニング業者に委託することができます。
マイニング業者はマイニング権委託されたTezos量に合わせたマイニング機会が得られ、
得られたマイニング報酬から手数料を引いて元の所有者に還元します。

この委託はマイニング権のみの委託です。実際のコインは所有者は保持したままです。

### マイナーの戦略

このため、Tezosでマイナーが多くのマイニング利益を得るためには、

* 多くのコインを保有する
* 良いデリゲーションサービスを提供することで他のコイン所有者からマイニング委託を受ける
* 保証金没収を避けるために正しくマイニングを行う

などの努力が必要です。

### 数字

Tezosのマイニングは特別なハードウェア(GPU, ASIC)を必要としません。
ラップトップ一台やRaspberry Pi 3でも可能です。

多く見積もってもこれらの設備の使用電力は100Wに届きません。
(代表的ASICは一台1500W以上かかる。) ノードが常にフル稼働することはありませんが、
100Wは一ヶ月でおよそ72kWh、ASIC一台の1/20以下で、日本での家庭での電気代にして
月に200円になりません。

ここから得られるマイニング収入は所有するコイン量に比例します。





