<!--
$ pandoc  --pdf-engine=lualatex --variable urlcolor=blue -o tezos_tech_info.pdf tezos_tech_info.md
-->
---
header-includes:
    -   \usepackage{amsmath}
geometry: margin=1in
documentclass: ltjarticle
papersize: a4
fontsize: 11pt
title: "Tezos技術文書について"
author: 古瀬 淳
date: \today
---

## Position paper, white paper

最初期のTezosの説明は、

* [Tezos: A Self-Amending Crypto-Ledger Position Paper](https://tezos.com/pdf/position_paper.pdf)
* [Tezos: A Self-Amending Crypto-Ledger White Paper](https://tezos.com/static/papers/white_paper.pdf)

になります。

Position paperは現状とあまりかわりません。Tezosの目標について知るためにお勧めです。

White paperは、赤字で書かれている更新部分も含めて、現状から離れている数字が多いです。また、短くまとめてあるせいか、説明が省略されているところも多い。現在はより拡充された文書が後述する https://tezos.gitlab.io/mainnet の White doc にありますので、この歴史的文書はわかりにくいところは読み飛ばす程度がよいです。

Position paper と white paper は[日本語に訳し](https://gitlab.com/dailambda/tezos-papers/tree/ja/pdfs)ました。細かいところは拙いかもしれませんが、だいたいあっているはずです。

## Developer document

現行のネットワークである mainnet を説明する技術文書は　https://tezos.gitlab.io/mainnet/ にあります。この文書のソースは Tezos のソースコードに同梱されています。

https://tezos.gitlab.io/master は master ブランチのソースから生成されたドキュメントなので、不完全であったり、mainnet とは違う部分があるので気をつけてください。

## Tezos ソースコード

レポジトリは GitLab にあります: https://gitlab.com/tezos/tezos 。

昔はGitHubを使っていたそうですが、現在は違うので注意してください。
現在は GitHub のレポは GitLab のミラーになっているはずです。

## 分野別重要記事

読むべき価値のある技術よりの内容の正しい記事を紹介します。

### Tzscan

[Tzscan](https://tzscan.io/)でブロックチェーン状態が見れます。

* [オペレーション量推移](https://tzscan.io/opsperday): 2018-11-18 から 2018-11-23 までスパム攻撃を受けていました。そのため緊急パッチを配布しています。

### LPoSについて

[TezosのPoSについて](https://medium.com/tezos/liquid-proof-of-stake-aec2f7ef1da7)
:    TezosのDPoSは他のEOSやTronなどのDPoSとは大きく違うところがあるので、
     この頃はLPoSと呼ぶようにしています。

### ハードフォーク、プロトコル改訂とその投票について

[Tezosでのハードフォークについて](https://medium.com/tezos/there-is-no-need-for-hard-forks-86b68165e67d)
:    Position paperと同じく、ビジョンを語っているので、抽象的なところがありますが、
     Tezosはハードフォークしない、などという幻想がわりとありますので、、、
	 Tezosブロックチェーンがダメになってしまうようなバグについては、
	 Tezosも普通に投票を経ない緊急ハードフォークを行います。

[Tezosでのプロトコル改訂の取り決め](https://medium.com/tezos/amending-tezos-b77949d97e1e)
:   プロトコロル改訂周りの手続きに関する非常に正確な技術記事です。

### スマートコントラクトについて

[Michelson仕様](http://tezos.gitlab.io/mainnet/whitedoc/michelson.html)
:   Tezosで使われている純粋関数型 stack VMの仕様です。
    [コントラクトオペレーション](http://tezos.gitlab.io/mainnet/whitedoc/michelson.html#operations-on-contracts)の部分以外は、ちょっと変わってはいるが単なるstack VMなので、
	stack VMを勉強した事がある人であれば普通に読む事ができるはずです。
	Michelsonは型が付いていますが、推論器以外はそんなに難しい物ではありません。

    https://www.michelson-lang.com/ は内容が古いので読まないように。

[Michelson Emacs mode](http://tezos.gitlab.io/alphanet/introduction/various.html?highlight=emacs#environment-for-writing-michelson-contracts)
:   Michelsonはstack VMなんですが、手書きすることもでき、支援する emacs mode があります。
    拙著の適当な[紹介記事](https://camlspotter.gitlab.io/blog/2018-10-12-michelson-mode/)もあります。

[Liquidity](http://www.liquidity-lang.org/)
:   Michelsonは流石に手で書くのは辛いので、OCamlに似たLiquidityという言語があり、
    ここからMichelsonにコンパイルできます。簡単なブラウザベースのコンパイラとネットワーク
	上での実行環境があります。

	LiquidityはTezosコアチームが開発した言語ではなく、外部企業のOCamlProが作ったものです。
	現在、コアチームではOCaml, Haskell, Python, Javascript風の言語から
	Michelsonへとコンパイルするコンパイラフレームワークを作成しているところです。
	が、それが出るまではLiquidityはTezosコミュニティーではスマートコントラクトを書く
	デファクトの言語でありつづけるでしょう。

スマートコントラクトを書くためのチュートリアルでTezos内部の人が書いたものは現在無いと思います。
主にコミュニティーの方が書いていますが、十分とは言えません:

[Contract a day](https://www.michelson-lang.com/contract-a-day.html)
:    いろいろあるんですが、残念ながら古いので現在の環境では動きません。

[Michelson tutorial](https://gitlab.com/camlcase-dev/michelson-tutorial/tree/master/01)
:    始まったばかりの Michelson のチュートリアルです。

[Liquidity examples](http://www.liquidity-lang.org/doc/tutorial/examples.html)
:    Liquidity で書かれたスマートコントラクトがいくつか。

### 理論的課題

Tezos foundation前理事で、現在Tezos Southeast Asiaの科学担当理事をしている
Diego Ponsによる研究者向けのブロックチェーン技術における理論的課題の説明です。
少々長いですが、、、

https://www.youtube.com/watch?v=GAOQUQ-15KY

## ノードランニングコスト

当然、PoSですので、計算は軽いです。GPUも使いません。

[RPI3でステーキングまで行う](https://github.com/maxtez-raspbaker/tezos-rpi3/wiki)例があります。

ラップトップで普通に baking できるので、最大 100W の電源がついているとしても(ラップトップとしては巨大です)1日2.4kWHです。1kWHが40円としても1日100円いきません。お手軽です。

## 他技術情報

全て現行のシステムについての情報です:

Blocktime
:    最速で1block/1分です。2018-12-20現在で約234700 block levelsあります。

DB size
:    現在40G強。ネットワーク稼働とトランザクション数からすると非常に大きいですが、DBのGCをしていないのが原因で、現在作業中です。 GCが終わると10Gほどになるとの報告があります:
     https://gitlab.com/tezos/tezos/merge_requests/720
	 この変更はDB実装部分の変更だけでプロトコルは変化しないので、問題なく近い将来導入されると思います。
	 
Block size
:    最大500kB。

TPS
:    最大性能40~45TPS。私の印象ではTezosは安易なTPS戦争に参加するつもりはないようです。
     安全性とのバーターなので、安全で売っている方向性と矛盾するので、、、

	 将来、プロトコル改訂を使って、blocktimeを短くするなどしてTPSを上げることは
	 可能ですが、それ以上のTPSを得るにはサイドチェーンなどの技術を使わないといけないでしょう。
	 Tezosコアチームはサイドチェーン開発には携わっておらず、コミュニティーで内で解決される
	 

     現在は[30000ops/day](https://tzscan.io/opsperday)なので、
	 全てtransacitonとして考えても 0.34TPSしか使われていません。

