# Tezos のソースコード中のドキュメントに関するメモ

この文書は[Tezosのソースコード](https://gitlab.com/tezos/tezos/tree/c61a1d02b95257c50c0b14b127efb4f84742edd2/docs)中のドキュメントについての簡単な解説である。
スースコードは commit `c61a1d02b95257c50c0b14b127efb4f84742edd2` を基にしている。

## ディレクトリ構成

### `api`

コマンドやAPIリファレンス。

`rpc.rst`
:    使用可能なRPCコールのリスト。自動生成されている。

`p2p.rst`
:    各データのバイナリ表現

`error.rst`
:    エラーの説明

### `introduction`

Tezosノードやクライアントの使い方入門書。

### `protocols`

各プロトコル変更点の解説。

### `tutorials`

プログラマーチュートリアル。
`michelson_anti_patterns.rst` 以外はノード開発者用の文書であり、和訳する必要はない。

`data_encoding.rst`
:    データのシリアライゼーションについて。

`entering_alpha.rst`
:    プロトコルのコードの読み方

`error_monad.rst`
:    エラーモナドについて。

`michelson_anti_patterns.rst`
:    こんな風に書いてはいけないMichelsonコード。要和訳。

`profiling.rst`
:    Tezosノードのプロファイリングについて

`protocol_environment.rst`
:    プロトコルがコンパイルされるべきライブラリ環境について

`rpc.rst`
:    RPCについて


### `whitedoc`

仕様書

`michelson.rst`
:    Michelsonについて。要和訳。

`proof_of_stake.rst`
:    TezosのPoSについて。要和訳。(すでに和訳してある)

`p2p.rst`
:    TezosのP2Pレイヤがどう動くのか。ノード内部のプログラマ対象。和訳する必要なし。

`the_big_picture.rst`
:    Tezosのプログラムの大雑把な構成。ノード内部のプログラマ対象。和訳する必要なし。

`validation.rst`
:    ブロックなどのデータバリデーションがどう動くのかについて。ノード内部のプログラマ対象。和訳する必要なし。

## 和訳優先順位

* `whitedoc/proof_of_stake.rst` (すでにしてある)
* `introduction/*` 以下のファイル
* `whitedoc/michelson.rst`
* `tutorials/michelson_anti_patterns.rst`
* `api/rpc.rst`
